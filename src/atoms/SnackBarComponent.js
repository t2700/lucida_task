import { Alert, Snackbar } from "@mui/material";
import React from "react";

function SnackBarComponent({ open, setOpen, status, content }) {
  // To close the Snackbar alert

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };
  return (
    <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
      <Alert onClose={handleClose} severity={status} sx={{ width: "100%" }}>
        {content}
      </Alert>
    </Snackbar>
  );
}

export default SnackBarComponent;
