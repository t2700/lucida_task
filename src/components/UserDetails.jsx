import React, { useEffect, useState } from "react";
import { Grid, Stack, Box, TextField, MenuItem, Button } from "@mui/material";
import { deleteByUserID } from "../services/index.js";
import TableComponent from "../atoms/TableComponent";
import CardComponent from "../atoms/CardComponent.js";
import { animal, animalHeader } from "./animal";
import { useNavigate } from "react-router-dom";
import DialogComponent from "../atoms/DialogComponent";
import SnackBarComponent from "../atoms/SnackBarComponent";

export default function Task2({
  data,
  rows,
  setRows,
  deleteID,
  setDeleteId,
  getAllUserData,
}) {
  const [genderDrop, setGenderDrop] = useState([]);
  const [statusDrop, setStausDrop] = useState([]);
  const [openModal, setOpenModal] = useState(false);
  const [openSnack, setOpenSnack] = useState(false);
  const [snackStatus, setSnackStatus] = useState({});
  const navigate = useNavigate();

  let rowHeader = ["Sl.No", "Id", "Name", "Email", "Gender", "Status"];

  //Api call
  useEffect(() => {
    getAllUserData();
  }, []);

  //onChange event for Gender
  const genderChange = (event) => {
    const {
      target: { value },
    } = event;
    setGenderDrop(typeof value === "string" ? value.split(",") : value);
  };
  //onChange event for Status
  const statusChange = (event) => {
    const {
      target: { value },
    } = event;
    setStausDrop(typeof value === "string" ? value.split(",") : value);
  };

  //Handling submit gender & status data
  const handleSubmit = () => {
    let copyGender =[...genderDrop];
    let copyStatus = [...statusDrop];
    let arr = [];

    if (copyGender.includes("all") || genderDrop.length === 0) {
      copyGender = ["male", "female"];
      setGenderDrop(["all"]);
    }
    if (copyStatus.includes("all") || statusDrop.length === 0) {
      copyStatus = ["active", "inactive"];
      setStausDrop(["all"]);
    }

    data.forEach((val) => {
      if (copyGender.includes(val.gender) && copyStatus.includes(val.status)) {
        arr.push(val);
      }
    });
    setRows(arr);
  };

  const handleConfirmDelete = () => {
    // API DELETE CALL
    deleteByUserID(deleteID)
      .then((res) => {
        if (res) {
          setOpenModal(false);
          setOpenSnack(true);
          getAllUserData();
          setSnackStatus({
            content: "Successfuly deleted",
            status: "success",
          });
        }
      })
      .catch((err) => {
        setSnackStatus({
          content: err.response.data.message,
          status: "error",
        });
        setOpenModal(false);
        setOpenSnack(true);
      });
  };

  return (
    <>
      {openModal && (
        <DialogComponent
          handleClickOpen={(id) => {
            setDeleteId(id);
            setOpenModal(true);
          }}
          openModal={openModal}
          setOpenModal={setOpenModal}
          description="Are you sure you want to delete the UserDetails?"
          handleConfirmDelete={handleConfirmDelete}
        />
      )}

      <Stack spacing={5}>
        <Grid container mt={2} spacing={1}>
          <Grid item xs={12} md={4} sm={6}>
            <Box width="100%">
              <TextField
                label="Select Gender"
                select
                value={genderDrop}
                onChange={genderChange}
                fullWidth
                SelectProps={{ multiple: true }}
                size="small"
                color="primary"
              >
                <MenuItem value="male">Male</MenuItem>
                <MenuItem value="female">Female</MenuItem>
                <MenuItem value="all">All</MenuItem>
              </TextField>
            </Box>
          </Grid>
          <Grid item xs={12} md={4} sm={6}>
            <Box width="100%">
              <TextField
                label="Select Status"
                select
                value={statusDrop}
                onChange={statusChange}
                fullWidth
                SelectProps={{ multiple: true }}
                size="small"
                color="primary"
              >
                <MenuItem value="active">Active</MenuItem>
                <MenuItem value="inactive">Inactive</MenuItem>
                <MenuItem value="all">All</MenuItem>
              </TextField>
            </Box>
          </Grid>
          <Grid item xs={12} md={4} sm={6}>
            <Button
              variant="contained"
              fullWidth
              size="medium"
              onClick={handleSubmit}
            >
              Submit
            </Button>
          </Grid>
        </Grid>
        <Grid container>
          <Grid>
            {/* <TableComponent rows={rows} rowHeader={rowHeader} /> */}
          </Grid>
          <Grid mt={2}>
            {/* <TableComponent rows={animal} rowHeader={animalHeader} /> */}
          </Grid>
        </Grid>
      </Stack>
      <Grid>
        <CardComponent
          rows={rows}
          title="User Details"
          onEditClick={(id) => {
            navigate(`/editform/${id}`);
          }}
          onDeleteClick={(id) => {
            setDeleteId(id);
            setOpenModal(true);
          }}
        />
      </Grid>
      <Grid mt={5}>
        {/* <CardComponent rows={animal} title="Animals Details" /> */}
      </Grid>
      {openSnack && (
        <SnackBarComponent
          open={openSnack}
          setOpen={setOpenSnack}
          status={snackStatus.status}
          content={snackStatus.content}
        />
      )}
    </>
  );
}
