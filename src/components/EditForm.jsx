import {
  Button,
  Card,
  CardActions,
  CardContent,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import SnackBarComponent from "../atoms/SnackBarComponent";
import CancelOutlinedIcon from "@mui/icons-material/CancelOutlined";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { updateByUserID } from "../services/index.js";

function EditForm({ rows, setRows, getAllUserData }) {
  const navigate = useNavigate();
  const [openSnack, setOpenSnack] = useState(false);
  const [snackStatus, setSnackStatus] = useState({});
  const [userNameErr, setUserNameErr] = useState(false);
  const [emailErr, setEmailErr] = useState(false);
  const [editRow, setEditRow] = useState({
    id: "",
    name: "",
    email: "",
    gender: "",
    status: "",
  });

  const { id } = useParams();
  useEffect(() => {
    getAllUserData();
  }, []);

  useEffect(() => {
    dataBind();
  }, [id, rows]);

  const dataBind = () => {
    for (let ele in rows) {
      if (rows[ele].id === Number(id)) {
        let val = rows[ele];
        setEditRow(val);
      }
    }
  };

  const handleUpdate = async () => {
    if (editRow.name && editRow.email) {
      await updateByUserID(id, editRow)
        .then((res) => {
          if (res) {
            setOpenSnack(true);
            setUserNameErr(false);
            setEmailErr(false);
            setSnackStatus({
              content: "Successfuly Updated",
              status: "success",
            });
            navigate("/task2");
          }
        })
        .catch((err) => {
          setOpenSnack(true);
          setUserNameErr(true);
          setEmailErr(true);
          setSnackStatus({
            content: err.response.data.message,
            status: "error",
          });
        });
    } else {
      editRow.name === "" ? setUserNameErr(true) : setUserNameErr(false);
      editRow.email === "" ? setEmailErr(true) : setEmailErr(false);
    }
  };
  const handleClose = () => {
    navigate("/task2");
  };
  return (
    <>
      <form>
        <Card sx={{ maxWidth: 600, marginLeft: 40, marginTop: 5 }}>
          <CardContent>
            <p>
              <CancelOutlinedIcon
                sx={{ marginLeft: 67 }}
                onClick={handleClose}
              />
            </p>
            <Typography gutterBottom variant="h6" component="div">
              User Details
            </Typography>
            <Grid
              container
              mt={5}
              alignItems="center"
              justify="center"
              direction="row"
              spacing={2}
            >
              <Grid item md={4}>
                <TextField
                  id="id-input"
                  name="id"
                  label="ID"
                  type="text"
                  size="medium"
                  disabled
                  value={editRow.id}
                  InputLabelProps={{ shrink: editRow.id }}
                />
              </Grid>
              <Grid item md={4}>
                <TextField
                  error={userNameErr}
                  helperText={userNameErr ? "Please enter User Name" : ""}
                  id="name-input"
                  name="name"
                  label="User Name"
                  type="text"
                  size="medium"
                  value={editRow.name}
                  onChange={(e) =>
                    setEditRow({ ...editRow, name: e.target.value })
                  }
                  InputLabelProps={{ shrink: editRow.name }}
                />
              </Grid>
              <Grid item md={4}>
                <TextField
                  error={emailErr}
                  helperText={emailErr ? "Please enter Email Id" : ""}
                  id="email-input"
                  name="email"
                  label="Email"
                  type="email"
                  size="medium"
                  value={editRow.email}
                  onChange={(e) =>
                    setEditRow({ ...editRow, email: e.target.value })
                  }
                  InputLabelProps={{ shrink: editRow.email }}
                />
              </Grid>
            </Grid>
            <Grid
              container
              mt={5}
              alignItems="center"
              justify="center"
              direction="row"
              spacing={2}
            >
              <Grid item md={6}>
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label">Gender</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={editRow.gender}
                    label="Gender"
                    size="medium"
                    onChange={(e) =>
                      setEditRow({ ...editRow, gender: e.target.value })
                    }
                  >
                    <MenuItem value="male">Male</MenuItem>
                    <MenuItem value="female">Female</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item md={6}>
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label">Status</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={editRow.status}
                    label="Status"
                    size="medium"
                    onChange={(e) =>
                      setEditRow({ ...editRow, status: e.target.value })
                    }
                  >
                    <MenuItem value="active">Active</MenuItem>
                    <MenuItem value="inactive">Inactive</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
            </Grid>
          </CardContent>
          <CardActions>
            <Button
              variant="contained"
              fullWidth
              size="medium"
              onClick={handleUpdate}
            >
              Update
            </Button>
          </CardActions>
        </Card>
      </form>
      {openSnack && (
        <SnackBarComponent
          open={openSnack}
          setOpen={setOpenSnack}
          status={snackStatus.status}
          content={snackStatus.content}
        />
      )}
    </>
  );
}

export default EditForm;
