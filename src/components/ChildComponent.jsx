import { Grid } from "@mui/material";
import React, { useEffect, useState } from "react";

function ChildComponent({ dataInputA, dataInputB, value, setValue }) {
  const [data, setData] = useState("");

  useEffect(() => {
    let oper = "";
    value === "addition"
      ? (oper = "+")
      : value === "multiplication"
      ? (oper = "*")
      : value === "subtraction"
      ? (oper = "-")
      : (oper = "");
    if (value !== "") {
      let text = `${Number(dataInputA)} ${oper} ${Number(dataInputB)}`;
      let result = eval(text);
      setData(result);
    }
  }, [value, dataInputA, dataInputB]);

  return (
    <Grid container sx={{ border: "1px solid black" }}>
      <Grid item xs={12} md={6} sm={6} p={2}>
        <label for="inputState">Select Operations</label>
        <select
          id="inputState"
          class="form-control"
          value={value}
          onChange={(e) => {
            setValue(e.target.value);
          }}
        >
          <option selected value="">
            Choose...
          </option>
          <option value="addition">
            Addition
          </option>
          <option value="subtraction">
            Subtraction
          </option>
          <option value="multiplication">
            Multiplication
          </option>
        </select>

        {data !== "" && (
          <p className="mt-2">
            {value} of {dataInputA ? dataInputA : 0} and{" "}
            {dataInputB ? dataInputB : 0} is {data}
          </p>
        )}
      </Grid>
    </Grid>
  );
}

export default ChildComponent;
