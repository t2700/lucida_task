import React from "react";
import {
  AppBar,
  Box,
  Toolbar,
  Typography,
  Stack,
  Avatar,
  IconButton,
  Tooltip,
  Button,
  Container,
} from "@mui/material";
import { Link } from "react-router-dom";
import AdbIcon from "@mui/icons-material/Adb";

export default function NavComponent() {
  return (
    <Stack>
      <AppBar position="static">
        <Container maxWidth="xl">
          <Toolbar disableGutters>
            <AdbIcon sx={{ display: { xs: "flex", md: "flex" }, mr: 1 }} />
            <Typography
              variant="h6"
              noWrap
              component="a"
              href="/"
              sx={{
                mr: 2,
                display: { xs: "none", md: "flex" },
                fontFamily: "monospace",
                fontWeight: 700,
                letterSpacing: ".3rem",
                color: "inherit",
                textDecoration: "none",
                outline: "none",
              }}
            >
              LOGO
            </Typography>

            <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "flex" } }}>
              <Link to="/" style={{ textDecoration: "none" }}>
                <Button
                  outline="none !important"
                  border="none !important"
                  sx={{
                    my: 2,
                    color: "white",
                    display: "block",
                    border: "none",
                  }}
                >
                  Calculation-Application
                </Button>
              </Link>
              <Link to="/task2" style={{ textDecoration: "none" }}>
                <Button
                  sx={{
                    my: 2,
                    color: "white",
                    display: "block",
                    textDecoration: "none",
                  }}
                >
                  User-Details
                </Button>
              </Link>
              <Link to="/task3" style={{ textDecoration: "none" }}>
                <Button
                  sx={{
                    my: 2,
                    color: "white",
                    display: "block",
                    textDecoration: "none",
                  }}
                >
                  Single-User
                </Button>
              </Link>
            </Box>

            <Box sx={{ flexGrow: 0 }}>
              <Tooltip title="Open settings">
                <IconButton sx={{ p: 0 }}>
                  <Avatar src="https://tse4.mm.bing.net/th/id/OIP.kyvcj1OBNnSYu8UCMAejTAHaIS?w=164&h=185&c=7&r=0&o=5&dpr=1.5&pid=1.7" />
                </IconButton>
              </Tooltip>
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
    </Stack>
  );
}
