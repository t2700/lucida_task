import "./App.css";
import ParentComponent from "./components/ParentComponent";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import React, { useState } from "react";
import Task2 from "./components/UserDetails";
import Task3 from "./components/SingleUser";
import EditForm from "./components/EditForm";
import { getUserDetails } from "./services";
import NavComponent from "./components/NavComponent";

function App() {
  const [deleteID, setDeleteId] = useState("");
  const [rows, setRows] = useState([]);
  const [data, setData] = useState([]);

  const getAllUserData = () => {
    getUserDetails()
      .then((res) => {
        if (res) {
          setData(res.data);
          setRows(res.data);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div className="App">
      <BrowserRouter>
        <NavComponent />
        <Routes>
          <Route exact path="/" element={<ParentComponent />} />
          <Route
            path="/task2"
            element={
              <Task2
                getAllUserData={getAllUserData}
                data={data}
                setData={setData}
                rows={rows}
                setRows={setRows}
                deleteID={deleteID}
                setDeleteId={setDeleteId}
              />
            }
          />
          <Route path="/task3" element={<Task3 />} />
          <Route
            path="/editform/:id"
            element={
              <EditForm
                getAllUserData={getAllUserData}
                rows={rows}
                setRows={setRows}
              />
            }
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
