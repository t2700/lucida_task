import {
  Button,
  Card,
  CardActions,
  CardContent,
  Grid,
  Typography,
} from "@mui/material";
import React from "react";

function CardComponent({ rows, title, onEditClick, onDeleteClick }) {
  return (
    <>
      <Grid container spacing={2}>
        {rows.map((result) => (
          <Grid item md={4} xs={12} sm={6}>
            <Card sx={{ maxWidth: 400, maxHeight: 300 }} key={result.id}>
              <CardContent>
                <Typography gutterBottom variant="h6" component="div">
                  {title}
                </Typography>
                {Object.keys(result).map((val) => (
                  <Typography variant="body2" color="text.secondary">
                    <label> {val.toUpperCase()}</label> : {result[val]}
                  </Typography>
                ))}
              </CardContent>
              <CardActions>
                <Button
                  size="small"
                  variant="outlined"
                  color="success"
                  onClick={() => {
                    onEditClick(result.id);
                  }}
                >
                  Edit
                </Button>
                <Button
                  size="small"
                  variant="outlined"
                  color="error"
                  onClick={() => {
                    onDeleteClick(result.id);
                  }}
                >
                  Delete
                </Button>
              </CardActions>
            </Card>
          </Grid>
        ))}
      </Grid>
    </>
  );
}

export default CardComponent;
