import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import React from "react";

function TableComponent({ rows, rowHeader }) {
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: "90vw" }} aria-label="simple table">
        <TableHead>
          <TableRow>
            {rowHeader.map((val, index) => {
              return <TableCell key={index}>{val}</TableCell>;
            })}
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row, index) => (
            <TableRow key={row.id}>
              <TableCell component="th" scope="row">
                {index + 1}
              </TableCell>
              {Object.keys(row).map((key) => (
                <TableCell component="th" scope="row">
                  {row[key]}
                </TableCell>
              ))}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

export default TableComponent;
