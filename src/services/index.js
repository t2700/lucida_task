import axios from "axios";
const getHeaders = () =>{
  return {
    headers: {
      Authorization:
        "Bearer c5de0ea2da67b6f6a6c71dfedda6b9d2c3caa437763c51f266e8cf489fae6e09",
    },
  }
}
const baseUrl = 'https://gorest.co.in/public/v2/users'
export const getUserDetails = () => {
  return axios.get(baseUrl);
};

export const getByUserID = (userId) => {
  return axios.get(`${baseUrl}/${userId}`);
};

export const deleteByUserID = (userId) => {
  return axios.delete(`${baseUrl}/${userId}`, getHeaders());
};

export const updateByUserID = (userId, obj) => {
  return axios.put(
    `${baseUrl}/${userId}`,
    { obj },
    getHeaders()
  );
};


