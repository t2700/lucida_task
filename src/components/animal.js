export const animal = [
  {
    "name": "Giraffa camelopardalis",
    "type": "mammal",
    "description": "Tall, with brown spots, lives in Savanna",
   
  },
  {
    "name": "Loxodonta africana",
    "type": "mammal",
    "description": "Big, grey, with big ears, smart",
    
  },
  {
    "name": "Trioceros jacksonii",
    "type": "reptile",
    "description": "Green, changes color, lives in 'East Africa'",
    
  }
]

export const animalHeader = ["Sl.No", "Name","Type", "Description" ];



