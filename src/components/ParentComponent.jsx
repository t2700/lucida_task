import { Grid } from "@mui/material";
import React, { useState } from "react";
import ChildComponent from "./ChildComponent";

export default function ParentComponent({}) {
  const [value, setValue] = useState("");
  const [dataInputA, setDataInputA] = useState("");
  const [dataInputB, setDataInputB] = useState("");
  const [inputA, setInputA] = useState("");
  const [inputB, setInputB] = useState("");

  // To set the data for inputA abd inputB
  const handleSubmit = (e) => {
    e.preventDefault();
    setDataInputA(inputA);
    setDataInputB(inputB);
  };

  // To reset the data for inputA abd inputB
  const handleReset = () => {
    setInputA("");
    setInputB("");
    setDataInputA("");
    setDataInputB("");
  };

  return (
    <>
      <Grid container spacing={2}>
        <Grid
          item
          xs={12}
          md={6}
          sm={6}
          m={5}
          sx={{ border: "1px solid black" }}
        >
          <form onSubmit={handleSubmit}>
            <Grid item xs={12} md={12} sm={12} p={2}>
              <label for="exampleInputEmail1">Enter First Value</label>
              <input
                type="number"
                class="form-control"
                required
                id=""
                placeholder="Enter First Value"
                value={inputA}
                onChange={(e) => {
                  setInputA(
                    Number(e.target.value) ? Number(e.target.value) : ""
                  );
                }}
              />
            </Grid>
            <Grid item xs={12} md={12} sm={12} p={2}>
              <label for="exampleInputPassword1">Enter Second Value</label>
              <input
                type="number"
                class="form-control"
                required
                placeholder="Enter Second Value"
                value={inputB}
                onChange={(e) => {
                  setInputB(
                    Number(e.target.value) ? Number(e.target.value) : ""
                  );
                }}
              />
            </Grid>
            <Grid
              item
              xs={12}
              md={12}
              sm={12}
              mb={3}
              p={2}
              sx={{ display: "flex", justifyContent: "space-between" }}
            >
              <button
                type="submit"
                className="btn btn-primary "
                onClick={handleReset}
              >
                Reset
              </button>
              <button type="submit" class="btn btn-primary">
                Submit
              </button>
            </Grid>
          </form>
        </Grid>
        <Grid item xs={12} md={5} sm={4} mt={4}>
          <ChildComponent
            dataInputA={dataInputA}
            setDataInputA={setDataInputA}
            dataInputB={dataInputB}
            setDataInputB={setDataInputB}
            value={value}
            setValue={setValue}
          />
        </Grid>
      </Grid>
    </>
  );
}
