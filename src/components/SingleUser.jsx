import {
  Alert,
  Button,
  Card,
  CardContent,
  Grid,
  Snackbar,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import { getByUserID } from "../services";

export default function Task3() {
  const [userId, setUserId] = useState("");
  const [result, setResult] = useState({});
  const [cardState, setCardState] = useState(false);
  const [open, setOpen] = useState(false);
  const [error, setError] = useState("");
  const [requireErr, setRequieErr] = useState(false);

  //to get user id and call API
  const handleSave = () => {
    if (userId) {
      getByUserID(userId)
        .then((res) => {
          if (res) {
            setResult(res.data);
            setCardState(true);
            setError("");
            setRequieErr(false);
          }
        })
        .catch((err) => {
          setOpen(true);
          setError(err.response.data.message);
          setCardState(false);
          setRequieErr(false);
        });
    } else {
      setRequieErr(true);
    }
  };

  return (
    <>
      <Stack mt={4} spacing={2}>
        <Grid container spacing={2}>
          <Grid item xs={12} md={4} sm={12}>
            <TextField
              error={requireErr}
              helperText={requireErr ? "Please enter UserId" : ""}
              type="number"
              id="outlined-basic"
              label="Enter UserID"
              size="small"
              variant="outlined"
              fullWidth
              onChange={(e) => setUserId(e.target.value)}
            />
          </Grid>
          <Grid item xs={12} md={4} sm={12}>
            <Button
              variant="contained"
              fullWidth
              size="medium"
              onClick={handleSave}
            >
              Submit
            </Button>
          </Grid>
        </Grid>
        <Grid container spacing={0}>
          <Grid item xs={12} sm={12} md={4} ml={6}>
            {cardState && (
              <Card sx={{ maxWidth: 345 }}>
                <CardContent>
                  <Typography gutterBottom variant="h6" component="div">
                    User Details
                  </Typography>
                  <Typography variant="body2" color="text.secondary">
                    <label> Name</label> : {result.name}
                  </Typography>
                  <Typography variant="body2" color="text.secondary">
                    <label>Email </label>: {result.email}
                  </Typography>
                  <Typography variant="body2" color="text.secondary">
                    <label> Gender </label>: {result.gender}
                  </Typography>
                  <Typography variant="body2" color="text.secondary">
                    <label> Status </label>: {result.status}
                  </Typography>
                </CardContent>
              </Card>
            )}
          </Grid>
        </Grid>
      </Stack>
      <Snackbar
        open={open}
        autoHideDuration={6000}
        onClose={(reason) => {
          if (reason === "clickaway") {
            return;
          }
          setOpen(false);
        }}
      >
        <Alert
          onClose={(reason) => {
            if (reason === "clickaway") {
              return;
            }
            setOpen(false);
          }}
          severity="error"
          sx={{ width: "100%" }}
        >
          {error}
        </Alert>
      </Snackbar>
    </>
  );
}
